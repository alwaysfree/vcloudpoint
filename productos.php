<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  

	<div id="divisor" class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Cliente Cero Introducción</li>
				</ol>
			</div>
		</div>
	</div>



	<div class="container">
		<h3 style="margin-top:40px;" class="col-12 text-center">Clientes Cero vCloudPoint</h3>
		<p style="margin-top:50px; margin-bottom:50px; color:#4a4a4a;">Los clientes cero vCloudPoint, brindan una forma innovadora de cómputo gracias
		a su software propietario y patentado vMatrix, entregando la misma experiencia de una PC de escritorio
		tradicional a un menor costo.
		</p>
	</div>

	<div class="container">
		<div  class="row" style="margin-bottom:50px;">
			<div class="col-md-1">
			</div>
			
			<div class="col-md-2" style="margin-bottom:5px;">
				<a href="productos.php" style="color:white; background-color:#0c96cc;"  class="boton-productos btn  btn-outline-danger activo">
					<i class="iconos-botones-productos fas fa-signal"></i> Conjunto
				</a>
			</div>

			<div  class="col-md-2" style="margin-bottom:5px;">
				<a id="menu"   href="beneficios.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos fas fa-certificate"></i> Beneficios</a>
			</div>

			<div  class="col-md-2" style="margin-bottom:5px;">
				<a id="menu"  href="reflejos.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos far fa-star"></i> Destacados</a>
			</div>

			<div class="col-md-2" style="margin-bottom:5px;">
				<a id="menu"   href="comparaciones.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos fas fa-ellipsis-h"></i> Comparaciones</a>
			</div>

			<div  class="col-md-2" style="margin-bottom:5px;">
				<a id="menu"   href="prueba-gratuita.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos far fa-hand-point-up"></i> Prueba gratis</a>
			</div>
			
			<div class="col-md-1">
			</div>
		</div>
	</div>

	<div class="iconos-centro">
		<i class="fas fa-sitemap pb-4"></i>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>Convierte 1 PC hasta en 30 estaciones de trabajo</h3>
				<p style="color:#4a4a4a;">
					Los Clientes Cero vCloudPoint permiten que multiples usuarios compartan simultánemente los 
					recursos de una PC de escritorio tradicional, entregando al mismo tiempo la misma experiencia.
				</p>

				<a style="margin-bottom:50px; margin-top:50px;" href="https://youtu.be/cmTRDUURWTc" target=_blank class="boton-productos btn  btn-outline-danger">
				  <i style="font-size:18px;" class="fas fa-caret-square-right"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="img/terminales.png" style="display:block; margin:0 auto;" class="img-fluid">
			</div>
		</div>
	</div>

	<div class="iconos-centro">
		<i class="fas fa-chart-bar pb-4"></i>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>
					Los clientes cero vCloudPoint presentan<br>
					una nueva forma de compartir una PC
				</h3>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="img/productos-vcloudpoint.jpg" style="display:block; margin:0 auto;" class="img-fluid pb-5">
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<p style="color:#4a4a4a; margin-bottom:40px;">
					Los clientes cero vCloudPoint, conocidos también como clientes ultra-ligeros, no contienen
					partes móviles ayudándole a tener ahorros considerables en costos de mantenimiento. Son 
					fáciles de configurar como de administrar, permitiéndole monitorear a todos sus usuarios en 
					tiempo real como también le permitirá modificar la experiencia audio - visual por cada usuario.
				</p>
			</div>
		</div>
	</div>


	<div class="container productos pb-5">
		<div class="row">
			<div class="col-md-7">
				<img src="img/demostracion-vcloudpoint.jpg" style="display:block; margin:0 auto;" class="img-fluid">
			</div>
			<div class="col-md-5">
				<p style="font-size:16px; margin-top:15px; color:#4a4a4a;">Cambie sus computadoras de escritorio por clientes cero de vCloudPoint y <b>Ahorre hasta: </b></p>
				<li style="font-size:16px; color:#4a4a4a; margin-left:50px;">70% en costo en mantenimiento</li><br>
				<li style="font-size:16px; color:#4a4a4a; margin-left:50px;">90% en consumo de energía</li><br>
				<li style="font-size:16px; color:#4a4a4a; margin-left:50px;">90% en actualizaciones</li><br>
				<a href="beneficios.php" style="display:inline-block;" class="boton-productos btn  btn-outline-danger">Leer más</a>
			</div>
		</div>
	</div>

	<div class="iconos-centro container">
		<i class="fas fa-user-cog"></i>
		<div>
			<h3 style="margin-top:30px;">¡Configurarlo y administrarlo es muy fácil</h3><br>
		</div>
		<div style="margin-top:-100px; margin-bottom:50px;">
			<a href="descargas/Guia-rapida-de-instalacion-S100.pdf" target=_blank class="boton-productos btn  btn-outline-danger">Ver guía de instalación</a>
		</div>
	</div>

	<div style="margin-top:30px;" class="container">
		<div class="row">
			<div class="col-12 col-md-4">
				<p style="color:orangered; font-size:24px; text-align:center; margin-bottom:10px;"><u>1</u></p>
				<p style="font-size:16px; text-align:center; color:#4a4a4a;">Instalación del software vMatrix</p><br>
				<img src="img/facil1.jpg" style="width:100%; display:block; margin: 0 auto; height:auto;">
			</div>
			<div class="col-12 col-md-4">
				<p style="color:orangered; font-size:24px; text-align:center; margin-bottom:10px;"><u>2</u></p>
				<p style="font-size:16px; text-align:center; color:#4a4a4a;">Crear cuentas de usuario</p><br>
				<img src="img/facil2.jpg" style="width:100%; display:block; margin: 0 auto; height:auto;" >
			</div>
			<div class="col-12 col-md-4">
				<p style="color:orangered; font-size:24px; text-align:center; margin-bottom:10px;"><u>3</u></p>
				<p style="font-size:16px; text-align:center; color:#4a4a4a;">Conectarse e Iniciar</p><br>
				<img src="img/facil3.jpg" style="width:100%; display:block; margin: 0 auto; height:auto">
			</div>
		  </div>
	</div>

	<div style="padding-top:80px;" class="iconos-centro container">
		<i class="fas fa-indent"></i>
		<div>
			<h3>
				Software vMatrix Server Manager
			</h3>
			<p style="color:#4a4a4a;">
				El software vMatrix Server Manager, se ejecuta desde una PC host, mejorando la funcionalidad
				ya que ofrece múltiples sesiones de usuario, aceleración gráfica, redirección de audio/usb, como una
				administración centralizada. El administrador puede configurar, monitorear y administrar las 
				sesiones de cada usuario, desde el lado del servidor a través de la consola.
			</p>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-5" style="margin-top:50px;">
				<p style="color:#4a4a4a;">
					Las funciones realizadas por el software vMatrix Server Manager son:
				</p>
                  
				<li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Brinda al administrador el control total de todas las sesiones de usuario
                </li>
				<li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
					Crear y administrar cuentas de usuario
				</li>
                <li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Modificar la experiencia audio-visual por usuario
                </li>
                <li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Monitoreo y control de las sesiones de usuario en tiempo real
                </li>
                <li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Configuración de los puertos USB y audio, como también de la resolución, video, etc.
                </li>
                <li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Compartir el escritorio del administrador a todos los usuarios
                </li>
                <li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Controlar la privacidad y la visibilidad de las particiones del disco para los usuarios
                </li>
                <li class="text-justify" style="margin-left:25px; color:#4a4a4a;">
                    Proporcionar una ruta de comunicación para administradores y usuarios a través del 
					chat integrado
                </li>
                <li class="text-justify" style="margin-left:25px; margin-bottom:40px; color:#4a4a4a;">
                    Mantener el control del comportamiento del usuario
                </li>
                <a href="descargas.php" target=_blank class="boton-productos btn  btn-outline-danger">Descargar gratis</a>
                
			</div>
			<div class="col-md-7">
				  <img style="width:600px; margin-top:50px;" src="img/funciones-softarevmatrix.jpg" class="img-fluid">
			</div>
		</div>
	</div>

	<div class="iconos-centro" style="margin-top:30px;">
		<i class="fas fa-indent"></i>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>
					Protocolo de escritorio dinámico (DDP)
				</h3>
				<p style="font-size:20px; color:#4a4a4a;">
					Los clientes cero de vCloudPoint utilizan su innovador protocolo de escritorio
					dinámico para la visualización de escritorios remotos. Este protocolo tiene una
					aceleración gráfica, un audio virtual, como también una redirección USB para brindar 
					una experiencia completa.
				</p>
				<p style="font-size:14px; color:#4a4a4a;">
					A diferencia de otros protocolos de virtualización, el DDP de vCloudPoint es completamente original
					permitiendo una compresión, transmisión y codificación de imagenes, esto para garantizar la capacidad
					de respuesta de la PC. En la transmisión de audio, el controlador DDP, admite audio bidireccional de
					entrada y salida, ofreciendo así la mejor calidad. <br> <br>
					Con el dispositivo USB DDP se logra la compatibilidad con la entrada de USB virtual, reconociendo en 
					unos pocos segundos cuando se está conectado, sin que sea necesario instalar un controlador. Con el 
					entorno multiusuario, los usuarios tendrán la seguridad para compartir su información. Por eso los 
					clientes cero vCloudPoint es la mejor solución.
				</p>
			</div>
		</div>
	</div>

	<div class="container" style="margin-bottom:50px;">
		<div class="row">
			<div class="col-md-7" class="img-fluid">
				<img src="img/art-oficina.jpg" style="width:100%; display:block; margin: 0 auto; height:auto;">
			</div>
			<div class="col-md-5">
				<p style="color:#4a4a4a;"><b>Video Full HD</b></p>
				<li style=" color:#4a4a4a; margin-left:40px;">Video HD local o video en línea</li>
				<li style=" color:#4a4a4a; margin-left:40px;">Permite cualquier formato, como también cualquier reproductor multimedia</li>
				<li style=" color:#4a4a4a; margin-left:40px;">Resolucion 1920 x 1080</li>
				<li style=" color:#4a4a4a; margin-left:40px;">30 videos simultáneos de 1080p con una PC i7</li>
				<br>

                <p style="color:#4a4a4a;"><b>Audio de alta calidad</b></p>
                <li style="color:#4a4a4a; margin-left:40px;">Audio de 16 bits 44.1 / 48 KHz de alta calidad</li>
                <li style="color:#4a4a4a; margin-left:40px;">Reproduzca y grabe en todos los sistemas operativos compatibles</li>
                <li style="color:#4a4a4a; margin-left:40px;">Totalmente sincronizado, solo 0.1 - 0.2 segundos de retraso</li>
                <li style="color:#4a4a4a; margin-left:40px;">Sin configuración, enchufar, conectar y usar</li>
				<br>

                <p style="color:#4a4a4a;"><b>Amplios dispositivos USB</b></p>
				<li style="color:#4a4a4a; margin-left:40px;">Admite una amplia gama de perifericos</li>
				<li style="color:#4a4a4a; margin-left:40px;">No se requiere controlador de cliente</li>
				<li style="color:#4a4a4a; margin-left:40px;">Enchufar, conectar y usar, funciona en cualquier momento.</li>
				<li style="color:#4a4a4a; margin-left:40px;">No hay problemas secundarios</li>
				<br>

                <div>
                    <a href="reflejos.php" target="_blank" class="boton-productos btn  btn-outline-danger">Ver más</a>
                </div>
			</div>
		</div>
	</div>

    <div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
