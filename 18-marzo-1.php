<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	
	
	<div class="row" style="background-color:#0b96ce; height:auto; margin-top:90px;">
		<div class="container">
			<div class="col-md-12">
				<p style="font-size:35px; color:white; text-align:left; margin-top:0px;">Nueva versión de VMATRIX 2.3.6</p>
				<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">18 de Marzo del 2019</p>
			</div>
		</div>
	</div>

	<div class="container" style="margin-bottom:10px;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Nueva versión de VMATRIX 2.3.6</li>
				</ol>
			</div>
		</div>
	</div>

	<div class="container" style="min-height:700px;">
        <div class="row">
			<div class="col-md-8">
				<!--<div class="row" style="height:400px; background:url(img/noticias/GITEX.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>-->
				<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
					La versión 2.3.6 de vMatrix Server Manager incluye correcciones importantes para mejorar la compatibilidad general y la confiabilidad de todo el sistema informático.<br><br>
					
					Nuevo: Se agregó el "Modo mejorado" en la pestaña "Configuración" para mejorar la compatibilidad de la aplicación.<br><br>

					Nuevo: se agregó la ventana emergente para solicitar que se reinicie el sistema después de desactivar la virtualización de IP.<br><br>

					Nuevo: se agregó la función de conexión automática del terminal después de que la sesión de escritorio se desconecte o cierre sesión en escenarios desatendidos.<br><br>

					Nuevo: los administradores pueden cargar y bloquear fondos de pantalla personalizados para dispositivos terminales en la administración del servidor.<br><br>

					Nuevo: se agregó soporte multilenguaje para el registro de administración.<br><br>

					Nuevo: agregada configuración de DNS para las terminales.<br><br>

					Nuevo: se agregó el icono de la consola de administración en el escritorio.<br><br>

					Nuevo: agregó una ventana emergente para mostrar iconos ocultos en la barra de tareas.<br><br>

					Nuevo: añadido idioma ucraniano.<br><br>

					Solucionado: modificó la lógica de prueba del controlador USB de la herramienta de diagnóstico para solucionar el problema de que el controlador USB es correcto pero el resultado de la prueba es incorrecto.<br><br>

					Solucionado: se solucionó el problema de que al copiar un archivo grande en un disco U conectado al dispositivo terminal se podían producir fallos en la sesión de escritorio.<br><br>

					Solucionado: solucionó el problema que los usuarios no pueden mostrar cuando el controlador de dominio actúa también como el host compartido.<br><br>

					Solucionado: se solucionó el error que al habilitar la virtualización de IP puede causar un bloqueo aleatorio de "DaemonService" y una falla de conexión del usuario.<br><br>

					Solucionado: solucionado el problema que el tiempo de inicio de sesión del usuario no se muestra en la pestaña "Administración de usuarios".<br><br>

					Solucionado: se solucionó el posible error .Net al cerrar la sesión de un usuario.<br><br>

					Solucionado: se solucionó el problema de cpaccel.exe en el host cuando el administrador inicia sesión justo después de cerrar la sesión.<br><br>

					Solucionado: se solucionó el problema de virtualización de IP que el dispositivo NIC seleccionado no se podía guardar en algunos casos, lo que provocaba que fallara la configuración de la IP virtual estática.<br><br>

					Solucionado: solucionó el problema de que el número de usuarios en línea no se actualizaba a tiempo.<br><br>

					Solucionado: solucionó el problema de que el nombre de la terminal que no se ha conectado al host no se muestra.<br><br> 

					Solucionado: solucionó el problema de error de inicio de sesión después de que un usuario cierra la sesión, lo que indica que no puede iniciar sesión repetidamente.<br><br>

					Solucionado: se solucionó el error de excepción de memoria después de cerrar la sesión de los usuarios en los sistemas Win7 / 08R2.<br><br>

					Corregido: corrige el rango válido de CPU y el porcentaje de uso de memoria que se muestra en la pestaña Administración de usuarios.<br><br>

					Solucionado: solucionó el problema de que no se podían mostrar los nombres chinos completos de los usuarios del sistema.<br><br>

					Solucionado: se solucionó el problema de "el host está ocupado" causado por el gran uso de la CPU de "DaemonService" en tiempo de inactividad.<br><br>

					Solucionado: la congelación aleatoria fija del servicio de host después de intentar iniciar sesión en un usuario.<br><br>

					Solucionado: se solucionó el error en Win7 / 8.1 de que los dispositivos no podían conectarse al host después de modificar el puerto de inicio del servicio de red en el administrador del servidor.<br><br>

					Solucionado: se solucionó el problema de que el cambio de idioma a través del icono inferior derecho no surta efecto.<br><br>

					Solucionado: se solucionó el problema en Win10 de que el contenido del video podía estar mal colocado cuando se reproducía con un reproductor VLC en el modo económico.<br><br>

					Mejorado: compatibilidad mejorada de redirección de dispositivos USB.<br><br>

					Mejorado: secuencia de asignación de IP virtual modificada en el inicio de sesión del usuario para reducir posibles conflictos de direcciones IP virtuales.<br><br>

					Mejorado: lógica de conexión de terminal optimizada en VPN para solucionar el problema de que los terminales no pueden iniciar sesión cuando se utiliza algún software VPN.<br><br>

					Mejorado: se modificó el diseño interno del menú "Configuración de red" dentro del menú del botón derecho de la pestaña "Administración de dispositivos" para permitir una configuración unificada de las direcciones IP de múltiples dispositivos terminales.<br><br>

					Mejorado: eliminó el menú separado de "Dirección IP fija" dentro del menú del botón derecho de la pestaña "Administración de dispositivos" para mover la configuración de la dirección IP fija al menú "Configuración de red".<br><br>

					Mejorado: reproducción de video optimizada en el reproductor de medios VLC.<br><br>

					Mejorado: la función de cambio de nombre se cambia para que se aplique a los usuarios sin conexión.<br><br>

					Mejorado: se eliminó el ícono del dispositivo de redirección USB en el host.<br><br>

			
				</p>
			</div>
			<div class="col-md-4">

	
	<p>Noticias Recientes</p>
	
	<a href="actualizacion-KB4471332.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		Aviso: La actualización del sistema de KB4471332 publicado el 11 de Diciembre causó 
		problemas en el inicio de sesión simultáneo de varios usuarios.
	</a>
	
	<hr>
	
	<a href="CEEIA-China.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT presente en la 75ª EXPO CEEIA en Nanchang China.
	</a>
	
	<hr>
	
	<a href="actualizacion-RDP.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		Aviso: Las últimas actualizaciones de Windows provocaron que RDP Wrapper no funcione.
	</a>
	
	<hr>
	
	<a href="premios-Globee-Awards.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT ha sido nombrada ganadora de oro y ganadora de plata en los premios Globee Awards.
	</a>
	
	<hr>
	
	<a href="GITEX2018.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT asistirá a GITEX 2018.
	</a>
	
</div>		</div>
	</div>


<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
