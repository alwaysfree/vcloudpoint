<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	
	<div class="row" style="background-color:#0b96ce; height:auto; margin-top:90px;">
		<div class="container">
			<p style="font-size:30px; color:white; margin-top:0px;">Noticias</p>
		</div>
	</div>

	<div class="container" style="margin-bottom:10px;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Compañia</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Noticias</li>
				</ol>
			</div>
		</div>
	</div>

	<div class="container">
        <div class="row">
			<div class="col-md-8">
				<table id="listado-noticias">

					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(img/noticias/noticias-cpaccel/1.png) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="18-abril.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Solución al "Error del controlador o CpAccel al iniciar sesión en vCloudPoint S100/V1".
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">31 de Julio del 2019</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							
							Todos los dispositivos USB no funcionan correctamente, el audio de la terminal no funciona y el rendimiento del video es malo (error del controlador o “CPACCEL”) / error al iniciar sesión en VCLOUDPOINT S100 / V1.[…]
						</p>
						<a class="btn btn-primary" href="31-julio.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>

					<hr style="margin-top:35px; margin-bottom:35px;">

					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(https://www.vcloudpoint.com/wp-content/uploads/RDS-vs-VDI-636x282.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="18-abril.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Conoces las diferencias entre RDS y VDI.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">24 de Junio del 2019</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							
							Las empresas aspiran a tecnologías que pueden ser eficientes y rentables para aumentar su productividad y satisfacer sus necesidades presentes y futuras. La virtualización de escritorio es un término de tendencia superior, que es capaz de ofrecer alto rendimiento, alta seguridad y flexibilidad requerida para todas las empresas. Los Servicios de Escritorio Remoto (RDS) o la infraestructura de Escritorio Virtual (VDI) puede lograr[…]
						</p>
						<a class="btn btn-primary" href="24-junio.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>

					<hr style="margin-top:35px; margin-bottom:35px;">

					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(https://www.vcloudpoint.com/wp-content/uploads/multi-user-login-problem1-636x448.png) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="18-abril.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Se solucionó el problema con la actualización de Windows que causaba el inicio de sesión de varios usuarios no funcionara.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">18 de Abril del 2019</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							
							Problema: después de instalar la actualización reciente del sistema de Windows, los hosts compartidos con RDP Wrapper no admiten el inicio de sesión de múltiples usuarios.[…]
						</p>
						<a class="btn btn-primary" href="18-abril.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>

					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(https://www.vcloudpoint.com/wp-content/uploads/CAAIE-member-636x453.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="18-marzo.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								vCloudPoint China fue otorgado como miembro de CEEIA y SZEEIA
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">18 de Marzo del 2019</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Shenzhen Cloudpoint Technology Co., Ltd (vCloudPoint China) se otorgó como miembro de la Asociación de la Industria de Equipos de Educación de China (CEEIA) y la Asociación de la Industria de Equipos de Educación de Shenzhen (SZEEIA), como reconocimiento del logro de vCloudPoint en la industria de la educación de China..[…]
						</p>
						<a class="btn btn-primary" href="18-marzo.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>

					<hr style="margin-top:35px; margin-bottom:35px;">

					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(https://www.vcloudpoint.com/wp-content/uploads/2.3.6-feature-image-636x477.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="18-marzo-1" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VMatrix 2.3.6 se lanza con una función de fondo de pantalla personalizada y correcciones importantes.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">18 de Marzo del 2019</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							La versión 2.3.6 de vMatrix Server Manager incluye correcciones importantes para mejorar la compatibilidad general y la confiabilidad de todo el sistema informático.[…]
						</p>
						<a class="btn btn-primary" href="18-marzo-1.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>

					<hr style="margin-top:35px; margin-bottom:35px;">

					<div class="col-md-12">
						<a href="actualizacion-KB4471332.php" style="font-size:20px; text-align:justify;">
							<p class="noticias-index">
								Aviso: La actualización del sistema de KB4471332 publicado el 11 de Diciembre causó 
								problemas en el inicio de sesión simultáneo de varios usuarios.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">17 de Diciembre del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							La actualización del sistema de KB4471332 para Windows 10 lanzado el 11 de diciembre causó un error en el 
							inicio de sesión simultáneo de varios usuarios al mismo tiempo. <br>
							Descripción: Cuando se ejecuta el archivo RDPConf de RDP Wrapper, se muestra como: <b>NO ESTÁ SOPORTADO</b>.<br>
							Causa: La actualización KB4471332 del sistema modificó la librería, lo que provocó que RDP Wrapper fallara. […]
						</p>
						<a class="btn btn-primary" href="actualizacion-KB4471332.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(img/noticias/CHINA.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="CEEIA-China.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VCLOUDPOINT presente en la 75ª EXPO CEEIA en Nanchang China.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">04 de Noviembre del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							vCloudPoint está mostrando una solución de computación basada en servidores mixtos en la 75ª EXPO de Equipos
							Educativos de China en la ciudad de Nanchang, China.[…]
						</p>
						<a class="btn btn-primary" href="CEEIA-China.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<a href="actualizacion-RDP.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Aviso: Las últimas actualizaciones de Windows provocaron que RDP Wrapper no funcione.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">13 de Septiembre del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Las actualizaciones de Windows publicadas actualmente pueden hacer que RDP Wrapper no funcioné.
							Dos de las actualizaciones conocidas son KB4343891 para Windows 8.1 / Server 2012 R2 y KB4343884 para Server 2016 (1607).[…]
						</p>
						<a class="btn btn-primary" href="actualizacion-RDP.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:900px; background:url(img/noticias/BEST.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="premios-Globee-Awards.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VCLOUDPOINT ha sido nombrada ganadora de oro y ganadora de plata en los premios Globee Awards.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">01 de Septiembre del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Shen Zhen Cloudpoint Technology Co., Ltd ha sido ganadora de Oro en la 5ta entrega anual de los 
							Globee Awards por "Crecimiento del cliente del año" y ganadora de Plata por "Socio y crecimiento de la distribución del año".[…]
						</p>
						<a class="btn btn-primary" href="premios-Globee-Awards.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(img/noticias/GITEX.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="GITEX2018.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VCLOUDPOINT asistirá a GITEX 2018.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">01 de Septiembre del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Gitex 2018, los nombres más reconocidos en tecnología, han cobrado vida en la semana de Tecnología de GITEX durante 38 años.
							Una semana mostrando la tecnología global y las innovaciones. VCLOUDPOINT presentará la solución de computación compartida
							que puede extender 1 PC I7 hasta en 30 estaciones de trabajo. Esperemos verlos en el stand A7-30 en Dubai World Trade.[…]
						</p>
						<a class="btn btn-primary" href="GITEX2018.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<a href="VMatrix-correcciones.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Nueva version: vMatrix 2.3.1, con correciones en el dominio y en Audio en el Sistema Operativo Windows 10 version 1803.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">16 de Agosto del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Se hicierón correciones de errores urgentes en el inicio de sesión del dominio y el Audio en el Sistema Operativo 
							Windows 10 versión 1803.[…]
						</p>
						<a class="btn btn-primary" href="VMatrix-correcciones.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:400px; background:url(img/noticias/BOLETIN.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="boletin-computacion.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								vCloudPoint Presente en la revista "Boletin de la computación".
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">01 de Agosto del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Con el fin de ayudar a las escuelas en este regreso a clases para mejorar sus centros de cómputo.[…]
						</p>
						<a class="btn btn-primary" href="boletin-computacion.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:250px; background:url(img/noticias/COMPUTER.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="ELECOMP2018.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VCLOUDPOINT presente en las 24ª EXPO ELECOMP en Irán.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">27 de Julio del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Feria Internacional de Tehran, Irán presente en el Stand No.440 mostrando el cliente cero V1 y en el Stand No.1134 mostrando el 
							modelo S100. <br>
							Acerca de ELECOMP: es el mejor evento comercial de productos y servicios electrónicos e informáticos en Irán.[…]
						</p>
						<a class="btn btn-primary" href="ELECOMP2018.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<a href="version1803-problemas.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Aviso: La actualización a la versión de Windows 10 1803 puede causar problemas inesperados.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">04 de Mayo del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							Como se anunció, la actualización de Windows 10 versión 1803, programada para abril de 2018, estuvo disponible para su descarga el 30 
							de abril. Sin embargo, vCloudPoint sugiere que los clientes no realicen una actualización a esta versión, ya que puede causar 
							varios problemas inesperados.[…]
						</p>
						<a class="btn btn-primary" href="version1803-problemas.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<a href="VCLOUDPOINT-cuernavaca-veracruz.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								Taller de terminales Cliente Cero VCLOUDPOINT en Cuernavaca y Veracruz.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">03 de Mayo del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							vCloudPoint y MAYCOM ofrecieron un taller de terminales Cliente Cero en los estados de Morelos y Veracruz.[…]
						</p>
						<a class="btn btn-primary" href="VCLOUDPOINT-cuernavaca-veracruz.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:360px; background:url(img/noticias/EDUCATIONAL.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="EXPOCEEIA.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VCLOUDPOINT asistiendo a la 74ª EXPO de Equipos de Cómputo Educativos de China.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">21 de Abril del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							VCLOUDPOINT asistirá a la 74ª EXPO de Equipos de Cómputo Educativos de China (CEEIA) que será del 11 al 13 de Mayo en el centro Internacional de
							Exposiciones de China. El CEEIA, que se celebra en dos sesiones cada año, es la feria comercial más grande de la industria educativa de China. En
							esta feria, los líderes educativos exploran las innovaciones a través de la tecnología que son […]
						</p>
						<a class="btn btn-primary" href="EXPOCEEIA.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
					<div class="col-md-12">
						<div class="row" style="height:360px; background:url(img/noticias/SVIAZ.jpg) center center no-repeat; padding-right:0px; padding-left:0px;"></div>
						<a href="EXPORUSIA.php" style="font-size:20px; text-align:justify;" class="card-title">
							<p class="noticias-index">
								VCLOUDPOINT se presentará en la EXPO


								SVIAZ 2018 realizado en Rusia.
							</p>
						</a>

						<p style="font-size:10px; text-align:left; margin-top:5px; color:#666;">2 de Abril del 2018</p>
						<p style="font-size:12px;  color:#666; margin-bottom:15px; " class="card-text">
							SVIAZ es una de las diez principales ferias de TIC en el mundo, con más de 40 años de historia la mayor feria de soluciones, productos de TIC y telecomunicaciones de Europa[…]
						</p>
						<a class="btn btn-primary" href="EXPORUSIA.php" role="button" style="font-family:arial; font-size:10px; background-color:#0b96ce;">Más Información</a>
					</div>
					
					<hr style="margin-top:35px; margin-bottom:35px;">
					
				</table>
            </div>
			<div class="col-md-4">

	
	<p>Noticias Recientes</p>
	
	<a href="actualizacion-KB4471332.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		Aviso: La actualización del sistema de KB4471332 publicado el 11 de Diciembre causó 
		problemas en el inicio de sesión simultáneo de varios usuarios.
	</a>
	
	<hr>
	
	<a href="CEEIA-China.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT presente en la 75ª EXPO CEEIA en Nanchang China.
	</a>
	
	<hr>
	
	<a href="actualizacion-RDP.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		Aviso: Las últimas actualizaciones de Windows provocaron que RDP Wrapper no funcione.
	</a>
	
	<hr>
	
	<a href="premios-Globee-Awards.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT ha sido nombrada ganadora de oro y ganadora de plata en los premios Globee Awards.
	</a>
	
	<hr>
	
	<a href="GITEX2018.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT asistirá a GITEX 2018.
	</a>
	
</div>        </div>	
	</div>

<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
