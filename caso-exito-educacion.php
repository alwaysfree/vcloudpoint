<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	
	
	<div class="row" style="background-color:#0b96ce; height:auto; margin-top:90px;">
		<div class="container">
			<div class="col-md-12">
				<p style="font-size:35px; color:white; text-align:left; margin-top:0px;">Casos de Éxito de VCLOUDPOINT "EDUCACIÓN".</p>
			</div>
		</div>
	</div>

	<div class="container" style="margin-bottom:10px;">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Casos de Éxito de Educación</li>
				</ol>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="tabbable-panel">
					<div class="tabbable-line">
						<div class="container">
							<ul class="nav nav-tabs">
								
									<li class="active" style="width:50%; text-align:center;">
										<a href="#tab_default_1" style="font-size:12px;" data-toggle="tab">
											Galeria 1
										</a>										
									</li>
								
									<li style="text-align:center; width:50%;">
										<a href="#tab_default_2" style="font-size:12px;" data-toggle="tab">
											Galeria 2
										</a>
									</li>
								<!--
									<li style="text-align:center; width:33.3%;">
										<a href="#tab_default_3" style="font-size:12px;" data-toggle="tab">
											Galeria 3 
										</a>
									</li>
								
								-->
								
							</ul>
						</div>
						
						<div class="tab-content">	
							<!--Inicio Sección 1 -->
							<div class="tab-pane active" id="tab_default_1">
								<div class="container" style="min-height:700px;">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Kuruprachasan/1.jpg" alt="">
														<div class="box-content">
															<h3 class="title">Tailandia</h3>
															<span class="post">Escuela Kuruprachasan</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#kuruprachasan" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#kuruprachasan" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="kuruprachasan" tabindex="-1" role="dialog" aria-labelledby="kuruprachasan" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="kuruprachasan">
																		Escuela Kuruprachasan en Tailandia 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Tailandia <br>
																			<b>Escuela:</b> Kuruprachasan<br>
																			<b>Sitio Web:</b> http://www.khu.ac.th <br>
																			<b>Persona de Contacto:</b> Mr. Viroch Prompang <br>
																			<b>Profesión:</b> Profesor <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 21 estaciones en 1 aula <br>
																			<b>Software:</b> Office, Dreamweaver, Photoshop <br>
																			<br>
																			Servidor usado para las 21 estaciones: <br>
																			-	<b>CPU:</b> Intel Core i7-7770<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco SSD:</b> 256 GB <br>
																			-	<b>Disco HDD:</b> 1 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-principal" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-principal" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-principal" data-slide-to="1"></li>
																					  <li data-target="#slider-principal" data-slide-to="2"></li>
																					  <li data-target="#slider-principal" data-slide-to="3"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Kuruprachasan/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Kuruprachasan/2.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Kuruprachasan/3.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Kuruprachasan/4.jpg" class="d-block img-fluid"></a>
																						</div><!--.carousel-item-->
															

																						<a href="#slider-principal" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-principal" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Colegio-militar-medicina/1.png" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Vietnam</h3>
															<span class="post">Colegio medico militar</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#medicina-vietnam" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#medicina-vietnam" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="medicina-vietnam" tabindex="-1" role="dialog" aria-labelledby="medicina-vietnam" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="medicina-vietnam">
																		Colegio medico militar en Vietnam
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Vietnam <br>
																			<b>Escuela:</b> Colegio medico militar en Vietnam<br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 100 estaciones en 4 aulas <br>
																			<b>Software:</b> Office, Aplicaciones Web, Internet <br>
																			<br>
																			Servidores usados: <br>
																			-	<b>Servidor:</b> 4 servidores HP Proliant DL20<br>
																			-	<b>CPU:</b> Intel Xeon CPU E3-1220 v5<br>
																			-	<b>Memoria:</b> 16 GB<br>
																			-	<b>Disco SSD:</b> 240 GB <br>
																			-	<b>Disco HDD:</b>  <br>
																			-	<b>Sistema Operativo:</b> Windows Server 2012 R2 Estandar<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-colegio-militar-medicina" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-colegio-militar-medicina" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-colegio-militar-medicina" data-slide-to="1"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Colegio-militar-medicina/1.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Colegio-militar-medicina/2.png" class="d-block img-fluid">
																						</div>
																						

																						<a href="#slider-colegio-militar-medicina" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-colegio-militar-medicina" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Instituto-computación-vietnam/1.png" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Vietnam</h3>
															<span class="post">Instituto de Computación</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#instituto-computacion-vietnam" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#instituto-computacion-vietnam" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="instituto-computacion-vietnam" tabindex="-1" role="dialog" aria-labelledby="instituto-computacion-vietnam" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="instituto-computacion-vietnam">
																		Instituto de Computación de Vietnam 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Vietnam <br>
																			<b>Escuela:</b> Instituto de Computación de Vietnam<br>
																			<b>Sitio Web:</b> http://vienmaytinh.com/ <br>
																			<b>Persona de Contacto:</b> Mr. Dai <br>
																			<b>Profesión:</b> Director <br>
																			<b>Modelo:</b> V1 y S100 <br>
																			<b>Total de estaciones instaladas:</b> 73 estaciones en una oficina <br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, etc. <br>
																			<br>
																			Servidor usado para las 21 estaciones: <br>
																			-	<b>CPU:</b> 2 x Intel Xeon E5-2695 v4<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco SSD:</b> 512 GB <br>
																			
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-instituto-computacion-vietnam" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-instituto-computacion-vietnam" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-instituto-computacion-vietnam" data-slide-to="1"></li>
																					  <li data-target="#slider-instituto-computacion-vietnam" data-slide-to="2"></li>
																					  <li data-target="#slider-instituto-computacion-vietnam" data-slide-to="3"></li>
																					  <li data-target="#slider-instituto-computacion-vietnam" data-slide-to="4"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Instituto-computación-vietnam/1.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Instituto-computación-vietnam/2.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Instituto-computación-vietnam/3.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Instituto-computación-vietnam/5.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Instituto-computación-vietnam/4.png" class="d-block img-fluid"></a>
																						</div><!--.carousel-item-->
															

																						<a href="#slider-instituto-computacion-vietnam" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-instituto-computacion-vietnam" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Vocacional-tecnica-Gungoren-Turquia/1.png" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Turquía</h3>
															<span class="post">Preparatoria Vocacional de Turquía</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#preparatoria-vocacional-tecnica-turquia" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#preparatoria-vocacional-tecnica-turquia" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="preparatoria-vocacional-tecnica-turquia" tabindex="-1" role="dialog" aria-labelledby="preparatoria-vocacional-tecnica-turquia" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="preparatoria-vocacional-tecnica-turquia">
																		Preparatoria Vocacional de Turquía 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Turquía <br>
																			<b>Escuela:</b> Preparatoria Vocacional y Técnica de Güngören<br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 30 estaciones de trabajo en don aulas de cómputo <br>
																			<b>Total de servidores instalados:</b> 2 <br>
																			<b>Software:</b> Office, Web, AutoCAD, SolidWorks, MasterCam, 3DMax <br>
																			<br>
																			Servidor usado para las 21 estaciones: <br>
																			-	<b>CPU:</b> AMD-FX-8350<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco SSD:</b> 240 GB <br>
																			
																			-	<b>Sistema Operativo:</b> Windows 7<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-preparatoria-vocacional-tecnica-turquia" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-preparatoria-vocacional-tecnica-turquia" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-preparatoria-vocacional-tecnica-turquia" data-slide-to="1"></li>
																					  <li data-target="#slider-preparatoria-vocacional-tecnica-turquia" data-slide-to="2"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Vocacional-tecnica-Gungoren-Turquia/1.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Vocacional-tecnica-Gungoren-Turquia/2.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Vocacional-tecnica-Gungoren-Turquia/3.png" class="d-block img-fluid">
																						</div>

																						<a href="#slider-preparatoria-vocacional-tecnica-turquia" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-preparatoria-vocacional-tecnica-turquia" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Nisarga-Batika-Nepal/1.png" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Nepal</h3>
															<span class="post">Escuela Nisarga Batika en Nepal</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Nisarga-Batika-Nepal" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Nisarga-Batika-Nepal" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Nisarga-Batika-Nepal" tabindex="-1" role="dialog" aria-labelledby="Nisarga-Batika-Nepal" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Nisarga-Batika-Nepal">
																		Escuela Nisarga Batika en Nepal 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Nepal <br>
																			<b>Escuela:</b> Escuela Nisarga Batika <br>
																			<b>Sitio Web:</b> http://www.nisarga.edu.np <br>
																			<b>Persona de Contacto:</b> Prabin Shrestha <br>
																			<b>Profesión:</b> Profesor de TI <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 44 estaciones en 2 aulas <br>
																			<b>Total de servidores instalados:</b> 2 <br>
																			<b>Software:</b> Office, Internet, Juegos Online, etc. <br>
																			<br>
																			Servidor usado para las 24 estaciones: <br>
																			-	<b>CPU:</b> E3-1230v6<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco SSD:</b> 480 GB <br>
																			-	<b>Disco HDD:</b> 2 x 1 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Nisarga-Batika-Nepal" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Nisarga-Batika-Nepal" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Nisarga-Batika-Nepal" data-slide-to="1"></li>
																					  <li data-target="#slider-Nisarga-Batika-Nepal" data-slide-to="2"></li>
																					  <li data-target="#slider-Nisarga-Batika-Nepal" data-slide-to="3"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Nisarga-Batika-Nepal/1.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Nisarga-Batika-Nepal/2.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Nisarga-Batika-Nepal/3.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Nisarga-Batika-Nepal/4.png" class="d-block img-fluid">
																						</div>
																						
															

																						<a href="#slider-Nisarga-Batika-Nepal" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Nisarga-Batika-Nepal" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Winston-México/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">México</h3>
															<span class="post">Colegio Sir Winston Leonard Spencer Churchill</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Winston-México" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Winston-México" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Winston-México" tabindex="-1" role="dialog" aria-labelledby="Winston-México" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Winston-México">
																		Colegio Winston en México 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> México <br>
																			<b>Escuela:</b> Colegio Sir Winston Leonard Spencer Churchill <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 93 estaciones en 3 aulas (2 aulas x 30 estaciones y 1 aula x 33 estaciones)<br>
																			<b>Total de servidores instalados:</b> 6 <br>
																			<b>Software:</b> Office, Internet, Visual Estudio, Adobe, etc. <br>
																			<br>
																			Servidor usado para las 93 estaciones: <br>
																			-	<b>CPU:</b> Intel Core i7-6700<br>
																			-	<b>Memoria:</b> 16 GB<br>
																			-	<b>Disco SSD:</b> 128 GB <br>
																			-	<b>Disco HDD:</b> 1 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 8.1<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Winston-México" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Winston-México" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Winston-México" data-slide-to="1"></li>
																					  <li data-target="#slider-Winston-México" data-slide-to="2"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Winston-México/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Winston-México/2.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Winston-México/3.jpg" class="d-block img-fluid">
																						</div>
																						
															

																						<a href="#slider-Winston-México" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Winston-México" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Universidad-artes-Irán/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Irán</h3>
															<span class="post">Universidad de Artes en Irán</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Universidad-artes-Irán" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Universidad-artes-Irán" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Universidad-artes-Irán" tabindex="-1" role="dialog" aria-labelledby="Universidad-artes-Irán" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Universidad-artes-Irán">
																		Universidad de Artes en Irán 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Irán <br>
																			<b>Escuela:</b> Universidad de Artes de Irán <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 40 estaciones en 3 aulas <br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, 3DMax, Adobe Ilustrator, Maya Software, Adobe InDesign, GIMP, etc. <br>
																			<br>
																			Servidor usado para las 40 estaciones: <br>
																			-	<b>CPU:</b> Intel Xeon E5-2670<br>
																			-	<b>Memoria:</b> 64 GB<br>
																			-	<b>Disco SSD:</b> 240 GB <br>
																			-	<b>Disco HDD:</b> 2 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Universidad-artes-Irán" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Universidad-artes-Irán" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Universidad-artes-Irán" data-slide-to="1"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Universidad-artes-Irán/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Universidad-artes-Irán/2.jpg" class="d-block img-fluid">
																						</div>

																						<a href="#slider-Universidad-artes-Irán" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Universidad-artes-Irán" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/1/Gems-Nepal/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Nepal</h3>
															<span class="post">Colegio Gems en Nepal</span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Gems-Nepal" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Gems-Nepal" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Gems-Nepal" tabindex="-1" role="dialog" aria-labelledby="Gems-Nepal" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Gems-Nepal">
																		Colegio Gems en Nepal 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Nepal <br>
																			<b>Escuela:</b> Colegio Gems en Nepal <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 43 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, DevC++, etc. <br>
																			<br>
																			Servidor usado para las 43 estaciones: <br>
																			-	<b>CPU:</b> Xeon E5-2630v4<br>
																			-	<b>Memoria:</b> 128 GB<br>
																			-	<b>Disco SSD:</b> 4 x 240 GB <br>
																			-	<b>Disco HDD:</b> 3 x 4 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 2012 R2<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Gems-Nepal" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Gems-Nepal" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Gems-Nepal" data-slide-to="1"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/1/Gems-Nepal/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/1/Gems-Nepal/2.jpg" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Gems-Nepal" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Gems-Nepal" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Fin Sección 1 -->
							
							<!-- Inicio Sección 2 -->
							<div class="tab-pane" id="tab_default_2">
								<div class="container" style="min-height:700px;">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Escuela-Erudit-Ucrania/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Ucrania</h3>
															<span class="post">Escuela Erudit de Ucrania </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Erudit-Ucrania" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Erudit-Ucrania" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Escuela-Erudit-Ucrania" tabindex="-1" role="dialog" aria-labelledby="Escuela-Erudit-Ucrania" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Escuela-Erudit-Ucrania">
																		Escuela Erudit de Ucrania 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Ucrania <br>
																			<b>Escuela:</b> Escuela Erudit de Ucrania <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 16 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, Software de Educación, etc. <br>
																			<br>
																			Servidor usado para las 16 estaciones: <br>
																			-	<b>CPU:</b> Intel Core I7-6700K<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco SSD:</b> 120 GB <br>
																			-	<b>Disco HDD:</b> 2 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 7<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Escuela-Erudit-Ucrania" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Escuela-Erudit-Ucrania" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Escuela-Erudit-Ucrania" data-slide-to="1"></li>
																					  <li data-target="#slider-Escuela-Erudit-Ucrania" data-slide-to="2"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Escuela-Erudit-Ucrania/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Erudit-Ucrania/2.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Erudit-Ucrania/3.jpg" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Escuela-Erudit-Ucrania" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Escuela-Erudit-Ucrania" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Escuela-Toseye-Iran/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Irán</h3>
															<span class="post">Escuela Toseye de Irán </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Toseye-Iran" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Toseye-Iran" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Escuela-Toseye-Iran" tabindex="-1" role="dialog" aria-labelledby="Escuela-Toseye-Iran" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Escuela-Toseye-Iran">
																		Escuela Toseye de Irán 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Irán <br>
																			<b>Escuela:</b> Escuela Toseye de Irán <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 10 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, Software de Educación, etc. <br>
																			<br>
																			Servidor usado para las 10 estaciones: <br>
																			-	<b>CPU:</b> Intel Core i5 -4690K<br>
																			-	<b>Memoria:</b> 8 GB<br>
																			-	<b>Disco SSD:</b> 64 GB <br>
																			-	<b>Disco HDD:</b> 1 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Escuela-Toseye-Iran" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Escuela-Toseye-Iran" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Escuela-Toseye-Iran" data-slide-to="1"></li>
																					  <li data-target="#slider-Escuela-Toseye-Iran" data-slide-to="2"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Escuela-Toseye-Iran/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Toseye-Iran/2.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Toseye-Iran/3.jpg" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Escuela-Toseye-Iran" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Escuela-Toseye-Iran" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Universidad-Shivaji-India/2.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">India</h3>
															<span class="post">Universidad Shivaji de la India </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Universidad-Shivaji-India" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Universidad-Shivaji-India" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Universidad-Shivaji-India" tabindex="-1" role="dialog" aria-labelledby="Universidad-Shivaji-India" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Universidad-Shivaji-India">
																		Universidad Shivaji de la India 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> India <br>
																			<b>Escuela:</b> Universidad Shivaji de la India  <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 10 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, Software de Educación, Photoshop, Diseño Web, CorelDraw, etc. <br>
																			<br>
																			Servidor usado para las 10 estaciones: <br>
																			-	<b>CPU:</b> Dell T430<br>
																			-	<b>Memoria:</b> 16 GB<br>
																			-	<b>Disco HDD:</b> 2 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Universidad-Shivaji-India" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Universidad-Shivaji-India" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Universidad-Shivaji-India" data-slide-to="1"></li>
																					  <li data-target="#slider-Universidad-Shivaji-India" data-slide-to="2"></li>
																					  <li data-target="#slider-Universidad-Shivaji-India" data-slide-to="3"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Universidad-Shivaji-India/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Universidad-Shivaji-India/2.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Universidad-Shivaji-India/3.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Universidad-Shivaji-India/4.jpg" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Universidad-Shivaji-India" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Universidad-Shivaji-India" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Jordan</h3>
															<span class="post">Academia Internacional Al Ettifaq </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Academia-Internacional-Al-Ettifaq" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Academia-Internacional-Al-Ettifaq" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Academia-Internacional-Al-Ettifaq" tabindex="-1" role="dialog" aria-labelledby="Academia-Internacional-Al-Ettifaq" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Academia-Internacional-Al-Ettifaq">
																		Academia Internacional Al Ettifaq 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> India <br>
																			<b>Escuela:</b> Academia Internacional Al Ettifaq  <br>
																			<b>Sitio Web:</b> http://www.alettifag.com/ <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 20 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, Software de Educación, etc. <br>
																			<br>
																			Servidor usado para las 20 estaciones: <br>
																			-	<b>CPU:</b> E3-1225 v6<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco HDD:</b> 2 TB <br>
																			-	<b>Sistema Operativo:</b> Windows 10<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Academia-Internacional-Al-Ettifaq" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Academia-Internacional-Al-Ettifaq" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Academia-Internacional-Al-Ettifaq" data-slide-to="1"></li>
																					  <li data-target="#slider-Academia-Internacional-Al-Ettifaq" data-slide-to="2"></li>
																					  <li data-target="#slider-Academia-Internacional-Al-Ettifaq" data-slide-to="3"></li>
																					  <li data-target="#slider-Academia-Internacional-Al-Ettifaq" data-slide-to="4"></li>
																					  <li data-target="#slider-Academia-Internacional-Al-Ettifaq" data-slide-to="5"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/2.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/3.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/4.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/5.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Academia-Internacional-Al-Ettifaq/6.png" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Academia-Internacional-Al-Ettifaq" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Academia-Internacional-Al-Ettifaq" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Escuela-Primaria-Jozef/3.png" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Eslovaquia</h3>
															<span class="post">Primaria Jozef de Eslovaquia </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Primaria-Jozef" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Primaria-Jozef" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Escuela-Primaria-Jozef" tabindex="-1" role="dialog" aria-labelledby="Escuela-Primaria-Jozef" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Escuela-Primaria-Jozef">
																		Primaria Jozef de Eslovaquia 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Eslovaquia <br>
																			<b>Escuela:</b> Primaria Jozef de Eslovaquia  <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 28 estaciones en 2 aula<br>
																			<b>Total de servidores instalados:</b> 2 <br>
																			<b>Software:</b> Office, Internet, Software de Educación, etc. <br>
																			<br>
																			Servidor usado para las 28 estaciones: <br>
																			-	<b>CPU:</b> Intel 6-core<br>
																			-	<b>Memoria:</b> 64 GB<br>
																			-	<b>Disco SDD:</b> 512 GB <br>
																			-	<b>Sistema Operativo:</b> Windows Server 2012R2<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Escuela-Primaria-Jozef" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Escuela-Primaria-Jozef" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Escuela-Primaria-Jozef" data-slide-to="1"></li>
																					  <li data-target="#slider-Escuela-Primaria-Jozef" data-slide-to="2"></li>
																					  <li data-target="#slider-Escuela-Primaria-Jozef" data-slide-to="3"></li>
																					  <li data-target="#slider-Escuela-Primaria-Jozef" data-slide-to="4"></li>
																					  <li data-target="#slider-Escuela-Primaria-Jozef" data-slide-to="5"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Escuela-Primaria-Jozef/1.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Primaria-Jozef/2.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Primaria-Jozef/3.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Primaria-Jozef/4.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Primaria-Jozef/5.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Primaria-Jozef/6.png" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Escuela-Primaria-Jozef" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Escuela-Primaria-Jozef" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/1.png" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Turquía</h3>
															<span class="post">Preparatoria Técnica de Turquía </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Preparatoria-Tecnica-Turquia" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Preparatoria-Tecnica-Turquia" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Preparatoria-Tecnica-Turquia" tabindex="-1" role="dialog" aria-labelledby="Preparatoria-Tecnica-Turquia" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Preparatoria-Tecnica-Turquia">
																		Preparatoria Técnica de Turquía 
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Turquía <br>
																			<b>Escuela:</b> Primaria Jozef de Eslovaquia  <br>
																			<b>Modelo:</b> S100 <br>
																			<b>Total de estaciones instaladas:</b> 20 estaciones en 2 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, SolidWorks, AutoCAD, etc. <br>
																			<br>
																			Servidor usado para las 20 estaciones: <br>
																			-	<b>CPU:</b> 2x AMD FX-8350<br>
																			-	<b>Memoria:</b> 32 GB<br>
																			-	<b>Disco SDD:</b> 240 GB <br>
																			-	<b>Sistema Operativo:</b> Windows 7<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Preparatoria-Tecnica-Turquia" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Preparatoria-Tecnica-Turquia" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Preparatoria-Tecnica-Turquia" data-slide-to="1"></li>
																					  <li data-target="#slider-Preparatoria-Tecnica-Turquia" data-slide-to="2"></li>
																					  <li data-target="#slider-Preparatoria-Tecnica-Turquia" data-slide-to="3"></li>
																					  <li data-target="#slider-Preparatoria-Tecnica-Turquia" data-slide-to="4"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/1.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/2.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/3.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/4.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/5.png" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Preparatoria-Tecnica-Turquia/6.png" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Preparatoria-Tecnica-Turquia" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Preparatoria-Tecnica-Turquia" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Escuela-primaria-Polonia/2.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Polonia</h3>
															<span class="post">Escuela Primaria en Polonia </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Escuela-primaria-Polonia" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Escuela-primaria-Polonia" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Escuela-primaria-Polonia" tabindex="-1" role="dialog" aria-labelledby="Escuela-primaria-Polonia" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Escuela-primaria-Polonia">
																		Escuela Primaria en Polonia
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Polonia <br>
																			<b>Escuela:</b> Escuela Primaria Wólka de Polonia  <br>
																			<b>Modelo:</b> V1 <br>
																			<b>Total de estaciones instaladas:</b> 20 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, SolidWorks, AutoCAD, etc. <br>
																			<br>
																			Servidor usado para las 20 estaciones: <br>
																			-	<b>CPU:</b> 2 x Intel Xeon Silver<br>
																			-	<b>Memoria:</b> 48 GB<br>
																			-	<b>Disco SDD:</b> 400 GB <br>
																			-	<b>Sistema Operativo:</b> Windows Server 2016<br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Escuela-primaria-Polonia" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Escuela-primaria-Polonia" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Escuela-primaria-Polonia" data-slide-to="1"></li>
																					  <li data-target="#slider-Escuela-primaria-Polonia" data-slide-to="2"></li>
																					  <li data-target="#slider-Escuela-primaria-Polonia" data-slide-to="3"></li>
																					  <li data-target="#slider-Escuela-primaria-Polonia" data-slide-to="4"></li>
																					  <li data-target="#slider-Escuela-primaria-Polonia" data-slide-to="5"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Escuela-primaria-Polonia/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-primaria-Polonia/2.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-primaria-Polonia/3.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-primaria-Polonia/4.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-primaria-Polonia/5.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-primaria-Polonia/6.jpg" class="d-block img-fluid">
																						</div>
															

																						<a href="#slider-Escuela-primaria-Polonia" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Escuela-primaria-Polonia" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												<div class="col-md-6 col-sm-6" style="margin-bottom:20px;">
													<div class="box7">
														<img src="img/escuelas/2/Escuela-Primaria-Rusia/1.jpg" style="height:208px;" alt="">
														<div class="box-content">
															<h3 class="title">Rusia</h3>
															<span class="post">Escuela Primaria en Rusia </span>
															<ul class="icon">
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Primaria-Rusia" class="fa fa-search"></a></li>
																<li><a href="#" data-toggle="modal" data-target="#Escuela-Primaria-Rusia" class="fa fa-link"></a></li>
															</ul>
														</div>
													</div>
													<div class="modal fade" id="Escuela-Primaria-Rusia" tabindex="-1" role="dialog" aria-labelledby="Escuela-Primaria-Rusia" aria-hidden="true">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<p class="modal-title" id="Escuela-Primaria-Rusia">
																		Escuela Primaria en Rusia
																	</p>
																</div>
																<div class="modal-body">
																	<div class="col-md-12">
																		<p style="font-size:15px; font-family:arial;">
																			<b>País:</b> Rusia <br>
																			<b>Escuela:</b> Escuela Primaria en Rusia  <br>
																			<b>Modelo:</b> V1 <br>
																			<b>Total de estaciones instaladas:</b> 11 estaciones en 1 aula<br>
																			<b>Total de servidores instalados:</b> 1 <br>
																			<b>Software:</b> Office, Internet, etc. <br>
																			<br>
																			Servidor usado para las 11 estaciones: <br>
																			-	<b>CPU:</b> Intel Core i3 2100<br>
																			-	<b>Memoria:</b> 8 GB<br>
																			-	<b>Disco SDD:</b> 240 GB <br>
																			
																		</p>
																		<div class="container">
																			<div class="row">
																				<div id="slider-Escuela-Primaria-Rusia" class="carousel slide mt-4" data-ride="carousel">
																					<ol class="carousel-indicators">
																					  <li data-target="#slider-Escuela-Primaria-Rusia" data-slide-to="0" class="active"></li>
																					  <li data-target="#slider-Escuela-Primaria-Rusia" data-slide-to="1"></li>
																					</ol>

																					<div class="carousel-inner" role="listbox">
																						<div class="carousel-item active">
																							<img src="img/escuelas/2/Escuela-Primaria-Rusia/1.jpg" class="d-block img-fluid">
																						</div>
																						<div class="carousel-item">
																							<img src="img/escuelas/2/Escuela-Primaria-Rusia/2.jpg" class="d-block img-fluid">
																						</div>
																						
															

																						<a href="#slider-Escuela-Primaria-Rusia" class="carousel-control-prev" data-slide="prev">
																							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																							<span class="sr-only">Anterior</span>
																						</a>
																						<a href="#slider-Escuela-Primaria-Rusia" class="carousel-control-next" data-slide="next">
																							<span class="carousel-control-next-icon" aria-hidden="true"></span>
																							<span class="sr-only">Despues</span>
																						</a>
																					</div><!--#slider-principal-->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
												
												
											</div>
										</div>
									</div>
								</div>	
							</div>
							<!-- Fin Sección 2 -->
							
							<!-- Inicio Sección 3 
							<div class="tab-pane" id="tab_default_3">
								<div class="container" style="min-height:700px;">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												
												
												
												
											</div>
										</div>
									</div>
								</div>	
							</div>
							
							Fin de Sección 3 -->

						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">

	
	<p>Noticias Recientes</p>
	
	<a href="actualizacion-KB4471332.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		Aviso: La actualización del sistema de KB4471332 publicado el 11 de Diciembre causó 
		problemas en el inicio de sesión simultáneo de varios usuarios.
	</a>
	
	<hr>
	
	<a href="CEEIA-China.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT presente en la 75ª EXPO CEEIA en Nanchang China.
	</a>
	
	<hr>
	
	<a href="actualizacion-RDP.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		Aviso: Las últimas actualizaciones de Windows provocaron que RDP Wrapper no funcione.
	</a>
	
	<hr>
	
	<a href="premios-Globee-Awards.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT ha sido nombrada ganadora de oro y ganadora de plata en los premios Globee Awards.
	</a>
	
	<hr>
	
	<a href="GITEX2018.php" style="font-size:12px; color:#666; text-align:justify;" class="card-title">
		VCLOUDPOINT asistirá a GITEX 2018.
	</a>
	
</div>		</div>
	</div>
	
	


<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
