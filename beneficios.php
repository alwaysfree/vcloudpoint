<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	<div id="divisor" class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Beneficios de los Clientes Cero</li>
				</ol>
			</div>
		</div>
	</div>

	<div class="container">
		<h3 style="margin-top:40px;" class="col-12 text-center">Clientes Cero vCloudPoint</h3>
		<p style="margin-top:50px; margin-bottom:50px; color:#4a4a4a;">Los clientes cero vCloudPoint, brindan una forma innovadora de cómputo gracias
		a su software propietario y patentado vMatrix, entregando la misma experiencia de una PC de escritorio
		tradicional a un menor costo.
		</p>
	</div>

	<div class="container">
		<div class="row" style="margin-bottom:50px;">
			<div class="col-md-1"></div>
			<div class="col-12 col-md-2  pt-4">
				<a id="menu"    href="productos.php"  class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos fas fa-signal"></i> Conjunto</a>
			</div>

            <div  class="col-12 col-md-2  pt-4">
				<a id="menu"   href="beneficios.php" class="boton-productos btn  btn-outline-danger activo">
                <i class="iconos-botones-productos fas fa-certificate"></i> Beneficios</a>
            </div>

			<div  class="col-12 col-md-2  pt-4">
				<a id="menu"  href="reflejos.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos far fa-star"></i> Destacados</a>
			</div>

			<div   class="col-12 col-md-2  pt-4">
				<a id="menu"   href="comparaciones.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos fas fa-ellipsis-h"></i> Comparaciones</a>
			</div>

			<div  class="col-12 col-md-2  pt-4">
				<a id="menu"   href="prueba-gratuita.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos far fa-hand-point-up"></i> Prueba gratis</a>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>

	<div class="iconos-centro">
		<i class="fas fa-share-square"></i>
		<h3 class="pt-4">Una forma innovadora de computación de escritorio</h3>
		<p class="text-center" style="font-size:22px; color:#4a4a4a;">Descubra los beneficios de reemplazar sus computadoras de escritorio por Clientes Cero vCloudPoint</p>
	</div>

	<div class="row" style="background-color:#efefef; height:auto;">
		<div class="container" style="background-color:#efefef; height:auto;">
            <div class="row" style="margin-right:15px; margin-left:15px;">
                <div class=" col-12  col-md-4" style="vertical-align:middle; margin:auto;">
                    <div class="iconos-centro hijo">
                        <i class="fas fa-dollar-sign"></i>
                        <h2 style="font-size:24px; color:black; font-family:sans-serif;">Reduciendo costos</h2>
                    </div>
                </div>
                <div class=" col-12 col-md-8" style="margin-bottom:10x;">
                    <p class="parrafo" style="font-size:14px; color:#4a4a4a; font-family:sans-serif; margin-top:38px; margin-bottom:38px;">
						<i class="fas fa-angle-right"></i><b> Ahorro en costos de adquisición:</b> 
							Los clientes cero vCloudPoint le permitirán tener un ahorro mayor al 50% en adquisición de equipos en 
							comparación con las computadoras tradicionales.
							<br><br>
						<i class="fas fa-angle-right"></i><b> Mayor aprovechamiento de los presupuestos de TI:</b> 
							Le permitirá compartir recursos de una PC, reduciendo los costos en la compra de licencias de software o 
							de hardware.
							<br><br>
						<i class="fas fa-angle-right"></i> <b> Ahorro de mantenimiento:</b> 
							Debido a que los clientes cero no cuentan con partes móviles, las reparación de un
							cliente cero es rara, por lo que usted reducirá un 90% en costos de mantenimiento.
							<br><br>
						<i class="fas fa-angle-right"></i><b> Ahorro de energía:</b> 
							Los clientes cero vCloudPoint solo consumen 5 watts en comparación con las computadoras
							de escritorios tradicionales que consumen 110 watts o más, por lo que con los clientes
							cero producira menos calor, proporcionando un ahorro del 90% en consumo de energía 
							<br><br>
						<i class="fas fa-angle-right"></i><b> Mayor tiempo de vida:</b> 
							Debido a que los clientes cero vCloudPoint no cuentan con partes móviles, la duración de vida de las terminales
							es más larga, siendo para empresas una opción viable.
                </div>
            </div>
        </div>
	</div>
	
    <div class="container" style="height:auto;">
		<div class="row" style="margin-left:20px; margin-right:20px;">
			<div class="col-md-8" style="margin-bottom:10x;">
				<p style="font-size:14px; color:#4a4a4a; font-family:sans-serif; margin-top:38px; margin-bottom:38px;">
					<i class="fas fa-angle-right"></i><b>Facilidad de Implementación:</b> 
					Con los Clientes Cero vCloudPoint la implementación de cientos de estaciones de trabajo 
					puede tomar algunas horas en comparación con las computadoras tradicionales que tardan
					días o semanas. Al instalar el software vMatrix usted podrá compartir los recursos de
					su PC servidor a todas sus estaciones de trabajo, donde podrá agregar varios usuarios; con
					solo conectar su mouse, teclado, monitor y el cable ethernet, los usuarios estarán listos para
					trabajar. Usted puede configurar una nueva estación de trabajo en segundos.
					<br><br>
				<i class="fas fa-angle-right"></i><b>Gestión centralizada:</b> 
					No hay nada que TI pueda configurar, administrar o actualizar en el punto final. Todas 
					las instalaciones y actualizaciones se realizan en la computadora servidor. 
					 <br><br>

				<i class="fas fa-angle-right"></i><b>Soporte técnico remoto:</b> 
					El personal de TI y soporte puede brindar soporte o solucionar problemas directamente 
					desde sus escritorios a través del monitoreo y control que se realiza sobre los escritorios de los 
					usuarios.
				</p>
			</div>
		  
			<div class="col-md-4 padre" style="vertical-align:middle; margin:auto;">
				<div class="iconos-centro hijo">
					<i class="fas fa-location-arrow"></i>
					<h2 style="font-size:24px; color:black; font-family:sans-serif;">Incrementando la productividad</h2>
				</div>
			</div>
		</div>
    </div>

    <div class="row" style="background-color:#efefef; height:auto;">    
		<div class="container" style="background-color:#efefef; height:auto;">
            <div class="row" style="margin-left:20px; margin-right:20px;">
                <div style="vertical-align:middle; margin:auto;" class="col-md-4 padre">
                    <div class="iconos-centro hijo">
                            <i class="fas fa-external-link-alt"></i>
                        <h2 style="font-size:24px; color:black; font-family:sans-serif;">Mejorando la eficiencia</h2>
                    </div>
                </div>
              
                <div style="margin-bottom:10x;" class="col-md-8">
                    <p style="font-size:14px; color:#4a4a4a; font-family:sans-serif; margin-top:38px; margin-bottom:38px;">
                    <i class="fas fa-angle-right"></i><b>Reducción del tiempo de inactividad no planificado:</b> 
						Sin partes móviles propensas a fallas y problemas, el tiempo de inactividad no planeado se 
						reduce en gran medida.
                    <br><br>
                    <i class="fas fa-angle-right"></i><b>Reinicio rápido en cualquier estación de trabajo:</b> 
						No se pierde tiempo en mover una computadora portátil pesada o reiniciar el sistema. Con 
						un simple cierre de sesión y luego iniciar sesión, 
						el usuario vuelve a su escritorio con todas las aplicaciones y archivos abiertos.
                    <br><br>
                    <i class="fas fa-angle-right"></i><b>Permitir el intercambio de archivos:</b> 
						Con algunos discos configurados visibles para todos, los usuarios pueden compartir y almacenar archivos fácilmente en estos discos públicos sin la necesidad de copiar o transferir entre escritorios.
                    </p>
                </div>
            </div>
        </div>
	</div>
        
	<div style="height:auto;" class="container">
		<div class="row" style="margin-left:20px; margin-right:20px;">
			<div style="margin-bottom:10x;" class="col-md-8">
				<p style="font-size:14px; color:#4a4a4a; font-family:sans-serif; margin-top:38px; margin-bottom:38px;">
				<i class="fas fa-angle-right"></i><b>Eliminación de la infección de virus:</b> 
					Sin usar un sistema operativo, ni una unidad de almacenamiento capaz de ejecutar algún software,
					los clientes cero vCloudPoint son inmunes a los virus y los malware.
					
				<br><br>
				<i class="fas fa-angle-right"></i><b>Políticas de seguridad y monitoreo:</b> 
					vMatrix permite al personal de TI aplicar políticas de seguridad, monitorear y deshabilitar 
					remotamente cualquier usuario final para iniciar sesión o simplemente bloquear periféricos USB 
					conectados localmente, evitando con ello que los usuarios copien datos confidenciales en 
					su USB.
				<br><br>
				<i class="fas fa-angle-right"></i><b>No hay riesgo de pérdida de datos:</b> 
					Como los clientes cero de vCloudPoint no pueden almacenar datos localmente, los usuarios 
					no tienen que preocuparse por la pérdida de datos derivada de fallas de hardware.
				</p>
			</div>
			
			<div style="vertical-align:middle; margin:auto;" class="col-md-4 padre">
				<div class="iconos-centro hijo">
				<i class="fas fa-shield-alt"></i>
					<h2 style="font-size:24px; color:black; font-family:sans-serif;">Mejorando la seguridad</h2>
				</div>
			</div>
		</div>
    </div>

	<div class="row" style="background-color:#efefef; height:auto;"> 
        <div style="background-color:#efefef; height:auto;" class="container">
            <div class="row" style="margin-left:20px; margin-right:20px;">
                <div style="vertical-align:middle; margin:auto;" class="col-md-4 padre">
                    <div class="iconos-centro hijo">
                    <i class="fas fa-leaf"></i>
                        <h2 style="font-size:24px; color:black; font-family:sans-serif;">Protección del medio ambiente</h2>
                    </div>
                </div>
                
                <div style="margin-bottom:15x;" class="col-md-8">
                    <p  style="font-size:14px; color:#4a4a4a; font-family:sans-serif; margin-top:38px; margin-bottom:38px;">
                        <i class="fas fa-angle-right"></i><b>Liberación de espacio:</b> 
							Los clientes cero de vCloudPoint son de un tamaño no mayor a 5 pulgadas 
							pudiendose montar en la parte posterior del monitor para ahorrar espacio 
							valioso en la mesa de trabajo.
                        <br><br>
                        <i class="fas fa-angle-right"></i><b>Sin ruido:</b> 
							Sin partes móviles integradas, los clientes cero de vCloudPoint no hacen ruido 
							cuando se esta ejecutando.
                        <br><br>
                        <i class="fas fa-angle-right"></i><b>Menos calor y desechos electrónicos :</b> 
							Consume mucha menos energía, lo que significa que los clientes cero vCloudPoint 
							producen mucho menos calor y desperdicio electrónico en comparación con las PC 
							tradicionales.
                    </p>
                </div>
            </div>
        </div>
	</div>



<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
