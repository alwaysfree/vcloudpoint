<!DOCTYPE html>
<html lang="en">
	<head>
		<title>vCloudPoint | Cliente Cero | Terminales vCloudPoint</title>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
			<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|PT+Sans:400,700|Roboto:400,700,900" rel="stylesheet">
			<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
			<link rel="stylesheet" href="css/styles.css">
			<link rel="stylesheet" href="css/menu.css">
			<link rel="stylesheet" href="css/footer.css">
			<link rel="stylesheet" href="css/container-inicio.css">
			<link rel="stylesheet" href="css/noticias.css">
			<link rel="stylesheet" href="css/caracteristicas.css">
			<link rel="stylesheet" href="css/divisor.css">
			<link rel="stylesheet" href="css/separador.css">
			<link rel="stylesheet" href="css/menu-activo.css">
			<link rel="stylesheet" href="css/formulario.css">
			<link rel="stylesheet" href="css/galeria.css">
			<link rel="stylesheet" href="css/imagen-hover.css">
			<link rel="stylesheet" href="css/nav-tabs.css">
			<link rel="stylesheet" href="css/productos.css">

			<link rel="icon" href="img/logo.ico">
	</head>

	<body onselectstart="return false">
		<nav class="navbar navbar-expand-md navbar-dark fixed-top" style="height:auto;">
			<div class="container">
				<div class="col-md-3">
					<a class="navbar-brand" href="index.php">
						<img src="img/logo vcloudpoint.png" style="margin:auto; width:200px; height:auto; padding:10px;" alt=""> 
					</a>

					<button style="border:none;  background-color:#0c96cc;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span style="background-color:#0c96cc;" class="navbar-toggler-icon">
						</span>
					</button>
				</div>

				<div class="col-md-9" style="margin-left:30px;">
					<div  class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active" style="margin-right:5px;">
								<a class="nav-link home" style="padding-right:0px; padding-left:0px;" href="index.php">
									<i class="fas fa-home"></i>
									<span class="sr-only">(current)</span>
								</a>
							</li>

							<li class="nav-item" style="margin-right:5px;">
								<a class="nav-link productos" style="padding-right:0px; padding-left:0px;" href="productos.php">
									<i class="fas fa-stop"></i>
								</a>
							</li>

							<li id="m-industria" style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#industria" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-building"></i>
								</a>
								<ul class="dropdown-menu" id="industria">
									<li>
										<a href="educacion.php">
											<i class="fa fa-graduation-cap"></i>
										</a>
									</li>

									<li>
										<a href="pymes.php">
											<i class="fas fa-briefcase"></i>
										</a>
									</li>

									<li>
										<a href="servicios.php">
											<i class="fas fa-comments"></i>
										</a>
									</li>

									<li>
										<a href="espacios-publicos.php">
											<i class="fas fa-cloud"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" class="dropdown">
								<a href="#" data-target="#soporte" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" 			aria-expanded="true">
									<i class="fas fa-user-shield"></i>
								</a>
								<ul class="dropdown-menu" id="soporte">
									<li>
										<a href="instalacion.php">
											<i class="fas fa-download"></i>
										</a>
									</li>
									<li>
										<a href="preguntas-frecuentes.php">
											<i class="far fa-question-circle"></i>
										</a>
									</li>
									<li>
										<a href="complementos.php">
											<i class="fas fa-plus"></i>
										</a>
									</li>
								</ul>
							</li>

							<li style="margin-top:12px; margin-right:5px;" 
								class="dropdown">
								<a href="#" data-target="#compania" style="padding-right:0px; padding-left:0px;" data-toggle="collapse" aria-expanded="true">
									<i class="far fa-calendar-alt"></i>
								</a>
								<ul class="dropdown-menu" id="compania">
									<li>
										<a href="nosotros.php">
											<i class="fa fa-industry"></i>
										</a>
									</li>
									<li>
										<a href="partner.php">
											<i class="fas fa-user-tie"></i>
										</a>
									</li>
									<li>
										<a href="noticias.php">
											<i class="far fa-newspaper"></i>
										</a>
									</li>
									<li>
										<a href="contacto.php">
											<i class="fa fa-phone-square"></i>
										</a>
									</li>
								</ul>
							</li>

							<li class="" style="padding-left:10px;">
								<a id="menu-casos" style="color:#666666 ; padding-right:0px; padding-left:0px;  font-size:13px; margin-top:8px;" class="nav-link casos-exito" href="casos_exito.php">
									Casos de éxito
								</a>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

  
	
	<div id="divisor" class="container">
		<div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb" style="background-color:transparent;">
					<li style="margin-top:-2px!important;">
						<a style="font-size:12px; color:#8b8b8b; " href="index.php">
							<img style="width:15px; margin-top:-6px;" src="img/glyphicons/png/home.png" alt="">  Inicio 
						</a>
					</li>
					<li class="active" style="font-size:12px;">&nbsp;&nbsp;<i class="fas fa-chevron-right"></i> Comparaciones</li>
				</ol>
			</div>
		</div>
	</div>


	<div class="container">
		<h3 style="margin-top:50px;" class="col-12 text-center">vCloudPoint Cliente Cero</h3>
		<p style="margin-top:50px; margin-bottom:50px; color:#4a4a4a;">Los clientes cero vCloudPoint, brindan una forma innovadora de cómputo gracias
			a su software propietario y patentado vMatrix, entregando la misma experiencia de una PC de escritorio
			tradicional a un menor costo.
		</p>
	</div><!--.container separacion-->

	<div class="container"  style="margin-top:50px; margin-bottom:50px;">
		<div  class="row">
			<div  class="col-md-1"></div>
			<div   class="col-12 col-md-2  pt-4">
				<a id="menu"    href="productos.php"  class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos fas fa-signal"></i> Conjunto</a>
			</div>

			<div  class="col-12 col-md-2  pt-4">
				<a id="menu"   href="beneficios.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos fas fa-certificate"></i> Beneficios</a>
			</div>

			<div  class="col-12 col-md-2  pt-4">
				<a id="menu"  href="reflejos.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos far fa-star"></i> Destacados</a>
			</div>

			<div   class="col-12 col-md-2  pt-4">
				<a id="menu"   href="comparaciones.php" class="boton-productos btn  btn-outline-danger activo">
				<i class="iconos-botones-productos fas fa-ellipsis-h"></i> Comparaciones</a>
			</div>

			<div  class="col-12 col-md-2  pt-4">
				<a id="menu"   href="prueba-gratuita.php" class="boton-productos btn  btn-outline-danger">
				<i class="iconos-botones-productos far fa-hand-point-up"></i> Prueba gratis</a>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	
	<div class="container">
		<h3 style="font-family:arial;">Tabla comparativa de los clientes cero VCloudPoint</h3>
			<div class="cardd" style="margin-bottom:20px;">
				<div class="row">
					<div class="preview col-md-6" style="border-right:1px solid #ccc;">
						<div class="container" style="border:3px solid #ccc;">
							<div class="preview-pic tab-content">
								<div class="tab-pane active thumbnail" id="pic-1">
									<img src="img/galeria/S100/s100-frente.jpg" />
								</div>
								<div class="tab-pane" id="pic-2"><img src="img/galeria/S100/s100-atras.png" /></div>
								<div class="tab-pane" id="pic-3"><img src="img/galeria/S100/s100-mano.png" /></div>
								<div class="tab-pane" id="pic-4"><img src="img/galeria/S100/s100-monitor.png" /></div>
								<div class="tab-pane" id="pic-5"><img src="img/galeria/S100/s100-trabajando.png" /></div>
								<div class="tab-pane" id="pic-6"><img src="img/galeria/S100/s100-partes.png" /></div>
							</div>
							<ul class="preview-thumbnail nav nav-tabs justify-content-center">
								<li class="active" ><a data-target="#pic-1" data-toggle="tab"><img src="img/galeria/S100/s100-frente.jpg" /></a></li>
								<li><a data-target="#pic-2" data-toggle="tab"><img src="img/galeria/S100/s100-atras.png" /></a></li>
								<li><a data-target="#pic-3" data-toggle="tab"><img src="img/galeria/S100/s100-mano.png" /></a></li>
								<li><a data-target="#pic-4" data-toggle="tab"><img src="img/galeria/S100/s100-monitor.png" /></a></li>
								<li><a data-target="#pic-5" data-toggle="tab"><img src="img/galeria/S100/s100-trabajando.png" /></a></li>
								<li><a data-target="#pic-6" data-toggle="tab"><img src="img/galeria/S100/s100-partes.png" /></a></li>
							</ul>
							<table class="table table-hover" style="margin-top:20px;">
								<tbody>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Modelo:</b> S100</td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Tamaño:</b> 4.7 Pulgadas</td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Entradas de video:</b> VGA </td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Ethernet:</b> 10/100 Mbps </td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Soporte de seguridad:</b> No incluido </td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
					
					
					<div class="preview col-md-6">
						<div class="container" style="border:3px solid #ccc;">
							<div class="preview-pic tab-content">
								<div class="tab-pane active thumbnail" id="pic-11">
									<img src="img/galeria/v1/v1-atras.jpg" />
								</div>
								<div class="tab-pane" id="pic-22"><img src="img/galeria/v1/v1-frente.png" /></div>
								<div class="tab-pane" id="pic-33"><img src="img/galeria/v1/v1-mano.png" /></div>
								<div class="tab-pane" id="pic-44"><img src="img/galeria/v1/v1-monitor.png" /></div>
								<div class="tab-pane" id="pic-55"><img src="img/galeria/v1/v1-montada.png" /></div>
								<div class="tab-pane" id="pic-66"><img src="img/galeria/v1/v1-partes.png" /></div>
							</div>
							<ul class="preview-thumbnail nav nav-tabs justify-content-center">
								<li class="active" ><a data-target="#pic-11" data-toggle="tab"><img src="img/galeria/v1/v1-atras.jpg" /></a></li>
								<li><a data-target="#pic-22" data-toggle="tab"><img src="img/galeria/v1/v1-frente.png" /></a></li>
								<li><a data-target="#pic-33" data-toggle="tab"><img src="img/galeria/v1/v1-mano.png" /></a></li>
								<li><a data-target="#pic-44" data-toggle="tab"><img src="img/galeria/v1/v1-monitor.png" /></a></li>
								<li><a data-target="#pic-55" data-toggle="tab"><img src="img/galeria/v1/v1-montada.png" /></a></li>
								<li><a data-target="#pic-66" data-toggle="tab"><img src="img/galeria/v1/v1-partes.png" /></a></li>
							</ul>
							<table class="table table-hover" style="margin-top:20px;">
								<tbody>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Modelo:</b> V1</td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Tamaño:</b> 3.5 Pulgadas</td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Entradas de video:</b> VGA y HDMI </td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Ethernet:</b> 10/100/1000 Mbps </td>
									</tr>
									<tr>
										<td style="font-size:15px; font-family:arial; color:#4a4a4a;"><b>Soporte de seguridad:</b> Incluye bracket antirrobo </td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-12">
						<p style="font-size:12px; text-align:center; font-family:arial;">*Haga click en las imagenes pequeñas para ver más grande</p>
					</div>
				</div>
			</div>
		
	</div>


<div class="container pt-4">
    <div class="row">
        <div class="col-12">
			<h3>Especificaciones de los Clientes Cero vCloudPoint</h3>
			  
			<div class="row">
				<div class="col-12 col-md-12 col-sm-12 col-xs-12">
					<table style="font-family:open sans; text-align:left;" class="table table-hover comparaciones">
						<thead class="thead-dark">
							<tr>
								<th style="font-size:12px; font-family:aria; color:white;"><b></b></th>
								<th style="font-size:12px; font-family:arial; color:white;">S100</th>
								<th style="font-size:12px; font-family:arial; color:white;">V1</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Tamaño:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">124 (ancho) x 75 (fondo) x26 (alto) mm</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">93 (ancho) x 90 (fondo) x18 (alto) mm</td>
							</tr>
							<tr>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Peso:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">107gr</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">150gr</td>
							</tr>
							<tr>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Color:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Negro</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Negro</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Entrada de video:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x VGA</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x HDMI, 1 x VGA</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Puertos USB:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">4 x USB 2.0</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">4 x USB 2.0</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Puerto de red:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x Ethernet RJ45</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x Ethernet RJ45</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Entrada de audio:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x Salida de Audio 3.5 mm</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x Salida de Audio 3.5 mm</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Salida de corriente:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">5v</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">5v</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Botón de Encendido:</b></td>
								<td style="text-align:left;font-size:12px; font-family:arial; color:#4a4a4a;">1 x Botón de encendido/reinicio</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1 x Botón de encendido/reinicio</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Bracket Antirrobo:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">No incluido</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Incluido</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Consumo de energía:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">5 watts</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">5 watts</td>
							</tr>
							<tr>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Resolución estándar (32 Bits):</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">640x480, 800x600, 1024x768, 1280x1024, 1600x1200</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">640x480, 800x600, 1024x768, 1280x1024, 1600x1200</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Resolución amplia (32 Bits):</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1360×76, 1366×768, 1440×900, 1600×900, 1680×1050, 1920×1080</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">1360×76, 1366×768, 1440×900, 1600×900, 1680×1050, 1920×1080</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Tipo de configuración:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Lado del servidor y del cliente</td>
								<td style="text-align:left;font-size:12px; font-family:arial; color:#4a4a4a;">Lado del servidor y del cliente</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Caracteristicas de audio:<b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Entrada y salida de audio con un tamaño de 3.5 mm</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Entrada y salida de audio con un tamaño de 3.5 mm</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Caracteristicas de video:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Full HD 1080px soportando varios formatos</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Full HD 1080px soportando varios formatos</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Cable de Red:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">10/100 Mbps Ethernet (RJ45)</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">10/100/1000 Mbps Ethernet (RJ45)</td>
							</tr>
							<tr>
							  <td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Red WIFI:</b></td>
							  <td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Antena WIFI externa opcional con costo adicional de 802.11b / g / n</td>
							  <td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Antena WIFI externa opcional con costo adicional de 802.11b / g / n</td>
							</tr>
							<tr>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Número máximo de usuarios por host compartido:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">vMatrix Server Manager permite hasta 30 usuarios por host compartido. El        número real de usuarios admitidos
								depende de la configuración del host y del tipo de aplicaciones utilizadas. </td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">vMatrix Server Manager permite hasta 30 usuarios por host compartido. El        número real de usuarios admitidos
								depende de la configuración del host y del tipo de aplicaciones utilizadas.</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Hardware interno:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">No cuenta con partes móviles, cómo son ventiladores y unidades de almacenamiento.</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">No cuenta con partes móviles, cómo son ventiladores y unidades de almacenamiento.</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Seguridad de datos:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">No contiene unidades almacenamiento de datos locales en el dispositivo. El acceso a los puertos USB son controlados por el administrador.</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">No contiene unidades almacenamiento de datos locales en el dispositivo. El acceso a los puertos USB son controlados por el administrador.</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Durabilidad:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Más de 100,000 horas</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Más de 100,000 horas</td>
							</tr>
							
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Certificaciones</b></td>
								<td style="text-align:center; font-size:12px; font-family:arial; color:#4a4a4a;" colspan="2">Clase A y B de FCC, CE, CCC, conformes con RoHS, BIS, NOM, ISO 9001: 2015, ISO 14001: 2015</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;"></td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Sistema que soporta:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Microsoft Windows XP SP3 Pro 32 bits, 7, 8, 8.1 y 10,
								  Server 2003 32 bits, 2008R2, 2012, 2012R2, 2016, Multipoint Server 2011, 2012</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">Microsoft Windows XP SP3 Pro 32 bits, 7, 8, 8.1 y 10,
								  Server 2003 32 bits, 2008R2, 2012, 2012R2, 2016, Multipoint Server 2011, 2012</td>
							</tr>
							<tr>
								<td style="text-align: left; font-size:12px; font-family:arial; color:#4a4a4a;"><b>Software incluido:</b></td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">vMatrix Server Manager con Dynamic Desktop Protocol (DDP)</td>
								<td style="text-align:left; font-size:12px; font-family:arial; color:#4a4a4a;">vMatrix Server Manager con Dynamic Desktop Protocol (DDP)</td>
							</tr>
						  
						</tbody>
					</table>
					<div class="pb-5">
						<a href="descargas/FICHA-TÉCNICA-V1.pdf" target="_blank" class="boton-index btn  btn-outline-danger">Descargar archivo</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row" style="background-color:#333333;">
	<div class="container">
		<div class="row" style="margin-right:15px; margin-left:15px;">
			<div class="col-md-3">
				<img class="img-fluid  pt-3 pb-3" src="img/logo vcloudpoint1.png ">
				<p class="text-justify pt-2 footer-descripcion">
					Las tecnologías vCloudPoint hacen que la informática de escritorio sea extremadamente simple y económica. Debido a la excelente
					flexibilidad y simplicidad de sus cliente cero, puede implementar, usar y administrar fácilmente escritorios para docenas
					a cientos de usuarios a una fracción de los costos.
				</p>
			</div>
		
			<div class="col-md-3">
				<p style="color:white;  font-size:20px;" class="pt-3 pb-3 text-center">Contacto</p>
				<a>
					<p class="text-center" style="font-size:13px;"><i class="far fa-envelope"></i>  ventas@vcloudpoint.com.mx</p>
				</a>
				<hr style="background-color:white;">
				<p class="text-center" style="font-size:14px;"><i class="fas fa-phone-volume"></i> 01 (55) 5615-2916</p>
			</div>

			<div class="col-md-3">
				<p style="color:white; font-family: sans-serif; font-size:20px;" class="pt-3 pb-3 text-center">Noticias recientes</p>
				<a href="18-abril.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>
						Aviso: Se solucionó el problema con la actualización del sistema de
						Windows que causaba el inicio de sesión de varios usuarios no funcionara.
					</p>
				</a>
				<p style="font-size:10px;">18 de Abril del 2019</p>
				<hr class="footer2">
				<a href="18-marzo.php">
					<p class="footer3 text-justify h5"><i class="fas fa-angle-right"></i>VCLOUDPOINT China fue otorgado como miembro de CEEIA y SZEEIA.</p>
				</a>
				<p style="font-size:10px;">18 de Marzo del 2019</p>
			</div>

			<div class="col-md-3">
				<br>
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:40px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="http://www.facebook.com/vCloudPointMX/"> <i class="fab fa-facebook-f"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://youtu.be/cmTRDUURWTc"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.linkedin.com/in/vcloudpoint-mexico-86a194168/"><i class="fab fa-linkedin-in"></i></a>
					</div>
				</div>
				
				<div class="col-xs-12 center-block" style="display:flex; margin-bottom:50px;">
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://twitter.com/vcloudmx?lang=es"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						<a target="_blank" href="https://www.instagram.com/vcloudpointmx/"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="col-md-4" style="text-align: center;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--barra-progreso-->
<script type="text/javascript">
	function animar (){
		document.getElementById("barra1").classList.toggle ("final1");
		document.getElementById("barra2").classList.toggle ("final2");
		document.getElementById("barra3").classList.toggle ("final3");

	}
	window.onload = function (){
	  animar();

	}
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
    $.src="https://v2.zopim.com/?51BGgA3dFcKcI1mPpd4Un7VFblV9TgM1";z.t=+new Date;$.
    type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


<footer style="background-color:#262626; height: auto;">
	<div class="container">
		<div class="row">
			<p style="color:#c5c5c5; font-size:14px;" class="text-center copyright w-100">vCloudPoint 2019. Todos los derechos reservados</p>
		</div> <!--.row-->
	</div><!--.container-->
</footer>









    <script src="js/jquery.slim.js"></script>
	<script src="js/app.js"></script>
    <script src="js/menu-activo.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/menu-hover.js"></script>
    <script src="js/validar-numero.js"></script>
  </body>
</html>
